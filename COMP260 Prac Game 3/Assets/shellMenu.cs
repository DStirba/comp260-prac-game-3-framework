﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shellMenu : MonoBehaviour {

    public GameObject optionsPanel;
    public Dropdown qualityDropdown;
    public Dropdown resolutionDropdown;
    public Toggle fullScreenToggle;
    public Slider volumeSlider;    public Slider musicSlider;    public AudioSource musicSource;

    public GameObject shellPanel;
    private bool paused = true;
    void Start()
    {

        // options panel is initially hidden
        optionsPanel.SetActive(false);
        SetPaused(paused);
        // populate the list of video quality levels
        qualityDropdown.ClearOptions();
        List<string> names = new List<string>();
        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            names.Add(QualitySettings.names[i]);
        }
        qualityDropdown.AddOptions(names);

        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            resolutions.Add(Screen.resolutions[i].ToString());
        }
        resolutionDropdown.AddOptions(resolutions);
        // restore the saved audio volume
        if (PlayerPrefs.HasKey("AudioVolume"))
        {
            AudioListener.volume =
            PlayerPrefs.GetFloat("AudioVolume");
        }
        else
        {
            // first time the game is run, use the default value
            AudioListener.volume = 1;
        }

        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            musicSource.volume =
            PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            // first time the game is run, use the default value
            musicSource.volume = 1;
        }
        musicSource.ignoreListenerVolume = true;

    }
    public void OnPressedOptions()
    {
        // show the options panel & hide the shell panel
        shellPanel.SetActive(false);
        optionsPanel.SetActive(true);

        qualityDropdown.value = QualitySettings.GetQualityLevel();

        // select the current resolution
        int currentResolution = 0;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            if (Screen.resolutions[i].width == Screen.width &&
            Screen.resolutions[i].height == Screen.height)
            {
                currentResolution = i;
                break;
            }
        }
        resolutionDropdown.value = currentResolution;        fullScreenToggle.isOn = Screen.fullScreen;        volumeSlider.value = AudioListener.volume;        musicSlider.value = musicSource.volume;
    }
    public void OnPressedCancel()
    {
        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }
    public void OnPressedApply()
    {
        QualitySettings.SetQualityLevel(qualityDropdown.value);
        Resolution res = Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(res.width, res.height, fullScreenToggle.isOn);
        AudioListener.volume = volumeSlider.value;        musicSource.volume = musicSlider.value;        PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
        PlayerPrefs.SetFloat("MusicVolume", musicSource.volume);
        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);

    }

    void Update()
    {
        // pause if the player presses escape
        if (!paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }
    }

    private void SetPaused(bool p)
    {
        // make the shell panel (in)active when (un)paused
        paused = p;
        shellPanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }

    public void OnPressedPlay()
    {
        // resume the game
        SetPaused(false);
    }

    public void OnPressedQuit()
    {
        // quit the game
        Application.Quit();
    }
}
